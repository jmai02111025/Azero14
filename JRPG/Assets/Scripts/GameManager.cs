using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Initial Variables

    public static GameManager instance = null;

    private GameObject bs;

    [HideInInspector]
    public PlayerController pc;

    [SerializeField]
    private List<CharacterBase> Base;

    public List<Character> Characters = new List<Character>();

    public List<Equipment> Weapons = new List<Equipment>();

    public List<Quest> acceptedQuests = new List<Quest>();

    public List<Quest> completedQuests = new List<Quest>();

    private GameObject enemy;

    public List<CharacterBase> EBase;

    public string enemyName;

    public bool destroyEnemy;

    public List<string> enemies;

    public Vector3 position;

    public Quaternion rotaiton;

    public Inventory inventory;

    public int coin;

    [SerializeField]
    Consumable hpPotion;

    [HideInInspector]
    public bool newScene;

    public string[] battlePosition = new string[9];

    #endregion

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // create characters
        for (int i = 0; i < Base.Count; i++)
        {
            SetUP(Base[i]);
        }
        inventory = new Inventory();
        coin = 0;
        inventory.AddConsumable(hpPotion, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (bs == null)
        {
            bs = GameObject.Find("BattleSystem");
        }
        if (bs != null && bs.activeSelf == true)
        {
            bs.GetComponent<BattleSystem>().SetCharacters(Characters);
        }

        // destroy enemies that are already dead
        if (SceneManager.GetActiveScene().name == "Scene1")
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                Destroy(GameObject.Find(enemies[i]));
            }
        }
        if (SceneManager.GetActiveScene().name == "TitleScene")
        {
            ResetGame();
        }
    }

    // setup character
    public void SetUP(CharacterBase Base)
    {
        Character c = new Character(Base);
        Characters.Add(c);
        switch (c.Base.CharacterName)
        {
            case "Arthur":
                c.Equipments.Add(Weapons[0]);
                break;
            case "Wendy":
                c.Equipments.Add(Weapons[1]);
                break;
            case "Zed":
                c.Equipments.Add(Weapons[2]);
                break;
            case "Arlia":
                c.Equipments.Add(Weapons[2]);
                break;
            default:
                break;
        }
        for (int i = 0; i < battlePosition.Length; i++)
        {
            if (battlePosition[i] == "")
            {
                battlePosition[i] = Base.CharacterName;
                break;
            }
        }
    }

    // get enemy information from the player controller
    public void GetEnemy(GameObject eGameObject)
    {
        enemy = eGameObject;
        EBase = enemy.GetComponent<EnemyBattleInformation>().Base;
        enemyName = enemy.name;
    }

    // reset the game
    public void ResetGame()
    {
        enemies.Clear();
        for (int i = 0; i < Characters.Count; i++)
        {
            Characters[i].HP = Characters[i].MaxHP;
            Characters[i].MP = Characters[i].MaxMP;
        }
    }
}
