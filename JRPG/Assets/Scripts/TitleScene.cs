using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // start a new game
    public void EnterNewGame()
    {
        SceneManager.LoadScene("Scene1");
    }

    // load scene
    public void LoadScene()
    {
        // load a save
        // put code here to load save data file when the save feature is implemented
    }

    // Exist game
    public void ExitGame()
    {
        Application.Quit();
    }
}
