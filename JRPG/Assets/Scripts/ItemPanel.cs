using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPanel : MonoBehaviour
{
    [SerializeField]
    private BattleSystem bs;

    [SerializeField]
    private List<Image> itemSlot;

    [SerializeField]
    private List<Text> itemText;

    [SerializeField]
    private List<Text> itemAmount1;

    [SerializeField]
    private List<Text> itemAmount2;

    [SerializeField]
    private Text description;

    private int currentItem;

    int i;

    private void OnEnable()
    {
        currentItem = 0;
        i = 0;
        // set all the data for the items on enable
        foreach (KeyValuePair<Item, int> item in GameManager.instance.inventory.inventory)
        {
            if (item.Key.itemType == Item.ItemType.Consumable)
            {
                itemText[i].text = item.Key.itemName;
                itemAmount1[i].text = item.Value.ToString();
                itemAmount2[i].text = item.Value.ToString();
                i++;
            }
        }

        for (int i = GameManager.instance.inventory.consumableList.Count; i < itemText.Count; i++)
        {
            itemText[i].text = "";
            itemAmount1[i].text = "";
            itemAmount2[i].text = "";
        }
    }

    // reset item panel
    private void OnDisable()
    {
        itemSlot[currentItem].color = new Color(0f, 0f, 0f, 0.5f);
    }

    private void Update()
    {
        if (i > 0)
        {
            ItemSelection();
        }
    }

    // function to choose item
    private void ItemSelection()
    {
        itemSlot[currentItem].color = Color.Lerp(Color.white, Color.yellow, Mathf.PingPong(Time.time, 1));
        description.text = "testing";

        if (Input.GetKeyDown(KeyCode.RightArrow) && currentItem < GameManager.instance.inventory.inventory.Count - 1)
        {
            itemSlot[currentItem].color = new Color(0f, 0f, 0f, 0.5f);
            currentItem++;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && currentItem > 0)
        {
            itemSlot[currentItem].color = new Color(0f, 0f, 0f, 0.5f);
            currentItem--;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && currentItem > 1)
        {
            itemSlot[currentItem].color = new Color(0f, 0f, 0f, 0.5f);
            currentItem -= 2;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && currentItem < GameManager.instance.inventory.inventory.Count - 2)
        {
            itemSlot[currentItem].color = new Color(0f, 0f, 0f, 0.5f);
            currentItem += 2;
        }
        if (Input.GetButtonDown("Submit") && GameManager.instance.inventory.inventory.Count != 0)
        {
            bs.turnIndicator.SetActive(false);
            bs.actionSeletor.SetActive(false);
            bs.targetSelector.SetActive(true);
            bs.targetSelector.GetComponent<TargetSelection>().attack = false;
            gameObject.SetActive(false);
        }
    }

    public string GetItem()
    {
        return itemText[currentItem].text;
    }
}
