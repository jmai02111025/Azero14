using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MPBar : MonoBehaviour
{
    [SerializeField]
    private GameObject mana;

    public void SetMP(float MP)
    {
        mana.transform.localScale = new Vector3(MP, 1f);
    }
}
