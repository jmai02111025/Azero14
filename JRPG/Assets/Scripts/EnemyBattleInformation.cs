using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBattleInformation : MonoBehaviour
{
    public List<CharacterBase> Base;

    public List<int> level;

    public int experience;
    
    // Start is called before the first frame update
    void Start()
    {
        
        experience = 0;
        for (int i = 0; i < level.Count; i++)
        {
            experience += level[i];
        }
        experience = level.Count * experience;
        
    }
}
