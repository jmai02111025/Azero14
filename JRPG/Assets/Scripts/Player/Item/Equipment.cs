using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "DefaultEquipment", menuName = "ScriptableObjects/Items/Equipment")]
public class Equipment : Item
{
    public enum EquipmentType
    {
        Weapon,
        Armor,
        Shoes,
        Extra1,
        Extra2,
    }

    public int STR;
    public int DEF;
    public int MGS;
    public int MDF;
    public int SPD;
    public int DEX;
    public int AGL;
    public int EVA;
    public int MaxHP;
    public int MaxMP;

    public int fireResist;
    public int waterResist;
    public int earthResist;
    public int windResist;
    public int holyResist;
    public int darknessResist;

    public EquipmentType equipmentType;
}
