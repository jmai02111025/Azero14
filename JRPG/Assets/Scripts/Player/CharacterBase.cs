using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "DefaultCharacter", menuName = "ScriptableObjects/Character", order = 3)]
public class CharacterBase : ScriptableObject
{
    [SerializeField] string characterName;

    [SerializeField] BaseType type;

    [SerializeField] NPCType NType;

    [SerializeField] GameObject characterHud;

    [SerializeField] GameObject characterPrefab;

    [SerializeField] Sprite elementIcon;

    [System.Serializable]
    public enum NPCType
    {
        Player,
        Wendy,
        Zed,
        Arlia,
        DSlime,
        ESlime,
    }

    // Base Stats
    [SerializeField] int STR;
    [SerializeField] int DEF;
    [SerializeField] int MGS;
    [SerializeField] int MDF;
    [SerializeField] int SPD;
    [SerializeField] int DEX;
    [SerializeField] int DEXBlind;
    [SerializeField] int AGL;
    [SerializeField] int EVA;
    [SerializeField] int luck;

    public bool isBlind = false;

    [SerializeField]
    private List<UniqueMagic> uniqueMagic;

    public string CharacterName
    {
        get
        {
            return characterName;
        }
    }

    public BaseType Type
    {
        get
        {
            return type;
        }
    }

    public int Str
    {
        get
        {
            return STR;
        }
    }

    public int Def
    {
        get
        {
            return DEF;
        }
    }

    public int Mgs
    {
        get
        {
            return MGS;
        }
    }

    public int Mdf
    {
        get
        {
            return MDF;
        }
    }

    public int Spd
    {
        get
        {
            return SPD;
        }
    }

    public int Dex
    {
        get
        {
            if (isBlind == true)
            {
                Debug.Log(isBlind);
                return DEXBlind;
            }
            else
            {
                Debug.Log(isBlind);
                return DEX;
            }
        }
    }

    public int Agl
    {
        get
        {
            return AGL;
        }
    }

    public int Eva
    {
        get
        {
            return EVA;
        }
    }

    public int Luck
    {
        get
        {
            return luck;
        }
    }

    public NPCType NpcType
    {
        get
        {
            return NType;
        }
    }

    public GameObject CharacterHud
    {
        get
        {
            return characterHud;
        }
    }

    public GameObject CharacterPrefab
    {
        get
        {
            return characterPrefab;
        }
    }

    public List<UniqueMagic> UniqueMagics
    {
        get
        {
            return uniqueMagic;
        }
    }

    public Sprite ElementIcon
    {
        get
        {
            return elementIcon;
        }
    }
}

[System.Serializable]
public class UniqueMagic
{
    [SerializeField]
    private MagicBase magicBase;

    [SerializeField]
    private int level;

    public MagicBase Base
    {
        get
        {
            return magicBase;
        }
    }

    public int Level
    {
        get
        {
            return level;
        }
    }
}

public enum BaseType
{
    Fire,
    Water,
    Earth,
    Wind,
    Holy,
    Darkness,
}

