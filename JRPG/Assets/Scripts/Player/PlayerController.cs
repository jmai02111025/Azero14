using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    #region Initial Variables
    [SerializeField]
    private float speed;

    [SerializeField]
    private Camera newCamera;

    private Animator animator;

    private Rigidbody2D rb;

    private Vector3 movement;

    private GameObject enemy;

    [SerializeField]
    private GameObject dialogueBox;

    private bool canTalk;

    [SerializeField]
    private GameObject statsMenu;

    [SerializeField]
    private GameObject tacticsMenu;

    [SerializeField]
    private float minScale;

    [SerializeField]
    private float maxScale;

    private float coefficient;

    private float newScale;

    [SerializeField]
    private GameObject transition;

    [HideInInspector]
    public GameObject NPC;

    [SerializeField]
    private DialogueSystem ds;

    [SerializeField]
    private float scaleModifier;

    #endregion

    private void OnEnable()
    {
        SceneManager.sceneLoaded += FindPlayer;
        if (GameManager.instance.enemyName != "")
        {
            enemy = GameObject.Find(GameManager.instance.enemyName);
            enemy.SetActive(false);
            EnemyResult(GameManager.instance.destroyEnemy);
            transform.position = GameManager.instance.position;
        }
        // if ran from battle
        if (enemy != null)
        {
            StartCoroutine(WaitForEnemy());
        }
        // fade in animation in new scene
        if (GameManager.instance.newScene == true)
        {
            transform.position = GameManager.instance.position;
            transform.rotation = GameManager.instance.rotaiton;
            transition.GetComponent<Animator>().Play("BlackOut");
            GameManager.instance.newScene = false;
        }
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= FindPlayer;
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // y position scaling
        if (newCamera != null)
        {
            coefficient = (transform.position.y + newCamera.GetComponent<CameraController>().bottom) / (newCamera.GetComponent<CameraController>().top - newCamera.GetComponent<CameraController>().bottom);
            newScale = ((minScale - maxScale) * coefficient + minScale) * scaleModifier;
            //gameObject.transform.localScale = new Vector3(newScale, newScale, newScale);
        }
        // when game is running
        if (Time.timeScale == 1f && GameManager.instance.newScene == false)
        {
            movement = Vector3.zero;
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");

            if (movement.x > 0)
            {
                transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            }
            else if (movement.x < 0)
            {
                transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
            }

            // talk to NPCs
            // open dialogue box if you can close enough to talk
            // close the dialogue if it's already opened.
            if (Input.GetButtonDown("Submit"))
            {
                if (NPC != null)
                {
                    if (dialogueBox.activeInHierarchy == false && canTalk == true)
                    {
                        ds.dm = NPC.GetComponent<DialogueManager>();
                        NPC.GetComponent<OverworldMovementInformation>().enabled = false;
                        animator.SetBool("isMoving", false);
                        NPC.GetComponent<Animator>().SetBool("isMoving", false);
                        dialogueBox.SetActive(true);
                        this.enabled = false;
                    }
                }
            }
        }

        // when game is paused
        if (Input.GetButtonDown("Cancel") && tacticsMenu.activeInHierarchy == false)
        {
            if (statsMenu.activeInHierarchy == false)
            {
                statsMenu.SetActive(true);
                Time.timeScale = 0f;
            }
        }

        // call tactics panel
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (tacticsMenu.activeInHierarchy == false)
            {
                tacticsMenu.SetActive(true);
                Time.timeScale = 0f;
            }
        }
    }

    void FixedUpdate()
    {
        // four direction movement and animation
        if (movement != Vector3.zero)
        {
            move();
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }
    }

    void move()
    {
        rb.MovePosition(transform.localPosition + movement * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // enter battle
        if (collision.gameObject.CompareTag("Enemy"))
        {
            enemy = collision.gameObject;
            GameManager.instance.position = transform.position;
            GameManager.instance.GetEnemy(enemy);
            StartCoroutine(EnterTrainsition("BattleScene"));
        }

        // can talk
        if (collision.gameObject.CompareTag("NPC"))
        {
            NPC = collision.gameObject;
            canTalk = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("NPC"))
        {
            canTalk = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.instance.position = collision.GetComponent<NewPositionOnTrigger>().position;
        GameManager.instance.rotaiton = collision.GetComponent<NewPositionOnTrigger>().rotation;

        // enter house
        if (collision.gameObject.name == "House1Trigger")
        {
            StartCoroutine(EnterTrainsition("HouseScene"));
            GameManager.instance.enemyName = "";
        }

        // enter town
        if (collision.gameObject.name == "TownTrigger")
        {
            StartCoroutine(EnterTrainsition("TownScene"));
            GameManager.instance.enemyName = "";
        }

        // back to scene 1
        if (collision.gameObject.name == "Scene1Trigger")
        {
            StartCoroutine(EnterTrainsition("Scene1"));
            GameManager.instance.enemyName = "";
        }

    }

    // disable the enemy with 5 seconds to allow the player to run away from the monster
    private IEnumerator WaitForEnemy()
    {
        yield return new WaitForSeconds(3f);
        if (enemy != null)
        {
            enemy.SetActive(true);
        }
    }

    // Do stuff with the enemy
    private void EnemyResult(bool result)
    {
        if (result == true)
        {
            GameManager.instance.enemies.Add(enemy.name);
        }
        else if (result == false)
        {
            enemy.SetActive(false);
        }
    }

    // fade out transition
    private IEnumerator EnterTrainsition(string sceneName)
    {
        movement = Vector3.zero;
        transition.GetComponent<Animator>().Play("BlackIn");
        GameManager.instance.newScene = true;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(sceneName);
    }

    // find this player controller script whenever a new scene is loaded
    private void FindPlayer(Scene scene, LoadSceneMode mode)
    {
        GameManager.instance.pc = this;
    }
}
