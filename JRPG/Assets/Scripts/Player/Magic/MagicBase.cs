using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultMagic", menuName = "ScriptableObjects/Magic", order = 2)]
public class MagicBase : ScriptableObject
{
    [SerializeField]
    string magicName;

    [SerializeField]
    MagicType magicType;

    [SerializeField]
    BaseType BaseType;

    // Works both as damage amount and support amount (e.g. how much it heals)
    [SerializeField]
    int basePower;

    [SerializeField]
    int cost;

    [SerializeField]
    DamageType DamageType;

    public StatusCondition statusCondition;

    public AttackDirection attackDirection;

    public WhichTarget whichTarget;

    public string MagicName
    {
        get
        {
            return magicName;
        }
    }

    public MagicType MType
    {
        get
        {
            return magicType;
        }
    }

    public BaseType BType
    {
        get
        {
            return BaseType;
        }
    }

    public int BasePower
    {
        get
        {
            return basePower;
        }
    }

    public int Cost
    {
        get
        {
            return cost;
        }
    }

    public DamageType DType
    {
        get
        {
            return DamageType;
        }
    }
}

public enum MagicType
{
    Damage,
    Support,
}

public enum DamageType
{
    Physical,
    Magic,
    Heal,
    StatChange,
}

public enum StatusCondition
{
    None,
    Burn,
    HealOverTime,
    Blind,
    Confuse,
    Reflect,
}

public enum AttackDirection
{
    Single,
    LineHorizontal,
    LineVertical,
    All,
}

public enum WhichTarget
{
    Enemies,
    Self,
    Allies,
}
