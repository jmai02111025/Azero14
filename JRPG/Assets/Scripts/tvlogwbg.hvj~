using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState
{
    Start,
    TurnRoll,
    PlayerAction,
    PlayerMove,
    EnemyMove,
    Result,
}

public class BattleSystem : MonoBehaviour
{
    #region Initial Variables
    bool selectionMade = false;
    bool magicSelected = false;

    public List<Character> playerUnits;

    public List<Character> enemyUnits = new List<Character>();

    public List<GameObject> characterHuds;

    [SerializeField]
    private List<GameObject> resultPrefabList;

    [SerializeField]
    private GameObject resultPrefab;

    public List<Text> actionText;

    public BattleState state;

    [SerializeField]
    private GameObject magicPanel;

    [SerializeField]
    private GameObject itemPanel;

    [SerializeField]
    private GameObject resultScreen;

    public GameObject actionSeletor;

    public int currentSelection;

    public List<GameObject> battleSprites;

    public Vector3[] playerSpawnPositions;

    public Vector3[] enemySpawnPositions;

    [SerializeField]
    private List<int> tempList;

    public GameObject turnIndicator;

    public GameObject targetSelector;

    public int moveIndex;

    private bool eMoved;

    private bool pMoved;

    private Transform parent;

    private int currentTarget;

    private bool magicAttack;

    public delegate void StateChange(BattleState battleStatus);
    StateChange stateChange;

    [SerializeField]
    TargetSelection targetSelection;
    #endregion
    private void OnEnable()
    {
        stateChange += ChangeState;
        playerUnits = GameManager.instance.Characters;
        CreateEnemy();
        state = BattleState.Start;
        StartCoroutine(SetUpBattle());
    }

    void CheckPlayersHP()
    {
        // destroy the character when the character's hp is less than 0
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i] != null)
            {
                if (playerUnits[i].HP <= 0)
                {
                    Destroy(battleSprites[i]);
                }
            }
        }
    }
    void CheckEnemiesHP()
    {
        // destroy the character when the character's hp is less than 0
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i] != null)
            {
                if (enemyUnits[i].HP <= 0)
                {
                    Destroy(battleSprites[i + playerUnits.Count]);
                }
            }
        }

    }

    void EnemyTurn()
    {
        // enemy action time
        if (eMoved == false)
        {
            magicAttack = false;
            currentTarget = Random.Range(0, playerUnits.Count);
            while (battleSprites[currentTarget] == null && currentTarget < playerUnits.Count)
            {
                currentTarget += 1;
                if (currentTarget >= playerUnits.Count)
                {
                    currentTarget = 0;
                }
            }
            StartCoroutine(Attack(battleSprites[moveIndex], playerSpawnPositions[currentTarget], 1.5f));
            eMoved = true;
            CheckPlayersHP();
        }
    }

    void BattleResult()
    {
        // result screen
        if (state == BattleState.Result)
        {
            resultScreen.SetActive(true);
            if (resultPrefabList.Count == 0)
            {
                for (int i = 0; i < playerUnits.Count; i++)
                {
                    // create result screen prefabs
                    parent = GameObject.Find("ResultScreen").transform;
                    GameObject resultIcon = Instantiate(resultPrefab, new Vector3(-334.1f, 250f - 100f * i, 0f), Quaternion.identity);
                    resultIcon.transform.SetParent(parent.transform, false);
                    resultPrefabList.Add(resultIcon);
                    resultPrefabList[i].GetComponent<ResultScreen>().setEXPData(playerUnits[i], GameManager.instance.Eexperience);
                }
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                GameManager.instance.destroyEnemy = true;
                SceneManager.LoadScene("Scene1");
            }
        }
    }

    void EnemyMove()
    {
        magicAttack = false;
        currentTarget = Random.Range(0, playerUnits.Count);
        while (battleSprites[currentTarget] == null && currentTarget < playerUnits.Count)
        {
            currentTarget += 1;
            if (currentTarget >= playerUnits.Count)
            {
                currentTarget = 0;
            }
        }
        StartCoroutine(Attack(battleSprites[moveIndex], playerSpawnPositions[currentTarget], 1.5f));
        eMoved = true;
    }

    //This is a function that will be used to change the states and certain variables that do and don't apply to them.
    void ChangeState(BattleState battleStatus)
    {
        switch (battleStatus)
        {
            case BattleState.Start:
                actionSeletor.SetActive(false);
                break;
            case BattleState.TurnRoll:
                Debug.Log("FENDER2");
                state = BattleState.TurnRoll;
                // decide who's turn is next
                moveIndex = NextMove();
                break;
            case BattleState.PlayerAction:
                // player selection time
                state = BattleState.PlayerAction;
                ActionSelection();
                break;
            case BattleState.PlayerMove:
                state = BattleState.PlayerMove;
                actionSeletor.SetActive(false);
                break;
            case BattleState.EnemyMove:
                state = BattleState.EnemyMove;
                actionSeletor.SetActive(false);
                if (eMoved == false)
                {
                    EnemyMove();
                }
                break;
            case BattleState.Result:
                actionSeletor.SetActive(false);
                BattleResult();
                break;
        }
    }

    private void Update()
    {
        selectionMade = false;
        stateChange(state);
        // choose target
        if (targetSelector.GetComponent<TargetSelection>().finishedSelection == true && state == BattleState.PlayerMove && pMoved == false)
        {
            currentSelection = 0;
            targetSelector.SetActive(false);
            currentTarget = targetSelector.GetComponent<TargetSelection>().GetTarget();
            StartCoroutine(Attack(battleSprites[moveIndex], enemySpawnPositions[currentTarget], 1.5f));
            pMoved = true;
        }
    }

    // set up battle, including icons, health bars, and mana bars
    public IEnumerator SetUpBattle()
    {
        targetSelection.numberOfEnemies = enemyUnits.Count - 1;
        for (int i = 0; i < playerUnits.Count; i++)
        {
            // create prefabs of character battle sprite
            GameObject PlayerSprite = Instantiate(playerUnits[i].Base.CharacterPrefab, playerSpawnPositions[i], Quaternion.identity);
            parent = GameObject.Find("BattleCanvas").transform;
            PlayerSprite.transform.SetParent(parent.transform, false);
            PlayerSprite.transform.localScale = new Vector3(20f, 20f, 20f);
            battleSprites.Add(PlayerSprite);

            // create prefabs of character hud, then set them to the correct stats.
            GameObject characterHud = Instantiate(playerUnits[i].Base.CharacterHud, new Vector3(-650f + i * 350f, -450f, 0f), Quaternion.identity);
            characterHud.transform.SetParent(parent.transform, false);

            characterHuds.Add(characterHud);

            characterHuds[i].GetComponent<CharacterHud>().SetData(playerUnits[i]);
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            // create prefabs of enemy battle sprite
            GameObject EnemySprite = Instantiate(enemyUnits[i].Base.CharacterPrefab, enemySpawnPositions[i], Quaternion.identity);
            EnemySprite.transform.SetParent(parent.transform, false);
            battleSprites.Add(EnemySprite);
        }

        // wait 1.5 seconds for characters to finish their intro animation
        yield return new WaitForSeconds(1.5f);


        stateChange(BattleState.TurnRoll);
    }

    // player choosing actions
    private void ActionSelection()
    {
        actionText[currentSelection].color = Color.Lerp(Color.white, Color.yellow, Mathf.PingPong(Time.time, 1));
        //Runs the function if the player pressed the up arrow, and a selection has not ben made.
        if (Input.GetKeyUp(KeyCode.DownArrow) && selectionMade == false)
        {
            selectionMade = true;
            if (currentSelection < 3)
            {
                actionText[currentSelection].color = Color.white;
                currentSelection++;
            }
        }
        //Runs the function if the player pressed the down arrow, and a selection has not ben made
        else if (Input.GetKeyUp(KeyCode.UpArrow) && selectionMade == false)
        {
            selectionMade = true;
            if (currentSelection > 0)
            {
                actionText[currentSelection].color = Color.white;
                currentSelection--;
            }
        }

        // open and show panels
        if (Input.GetKeyUp(KeyCode.Return) && selectionMade == false && magicSelected == false)
        {
            magicSelected = true;
            selectionMade = true;
            // attack
            if (currentSelection == 0)
            {
                turnIndicator.SetActive(false);
                actionSeletor.SetActive(false);
                targetSelector.SetActive(true);
                targetSelector.GetComponent<TargetSelection>().attack = true;
                magicAttack = false;
            }
            // magic panel
            if (currentSelection == 1)
            {
                magicPanel.SetActive(true);
                actionText[currentSelection].color = Color.white;
                actionSeletor.SetActive(false);
                magicAttack = true;
            }
            // item panel
            if (currentSelection == 2)
            {
                itemPanel.SetActive(true);
                actionText[currentSelection].color = Color.white;
                actionSeletor.SetActive(false);
            }
            // run
            if (currentSelection == 3)
            {
                for (int i = 0; i < playerUnits.Count; i++)
                {
                    playerUnits[i].HP = Mathf.FloorToInt(0.9f * playerUnits[i].HP);
                    playerUnits[i].MP = Mathf.FloorToInt(0.9f * playerUnits[i].MP);
                }
                SceneManager.LoadScene("Scene1");
                GameManager.instance.destroyEnemy = false;
            }
        }
        // close and hide panels
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            magicPanel.SetActive(false);
            actionSeletor.SetActive(true);
            itemPanel.SetActive(false);
            magicSelected = false;

            if (targetSelector.active == true)
            {
                if (magicAttack == true)
                {
                    magicPanel.SetActive(true);
                }
                if (magicAttack == false)
                {
                    actionSeletor.SetActive(true);
                }
                targetSelector.SetActive(false);
                turnIndicator.SetActive(true);
            }
        }
        CheckEnemiesHP();
    }

    // attack animation
    private IEnumerator Attack(GameObject go, Vector3 targetPosition, float seconds)
    {
        // go to target position
        float elapsedTime = 0;
        Vector3 startingPosition = go.transform.localPosition;

        while (elapsedTime < seconds)
        {
            go.transform.localPosition = Vector3.Lerp(startingPosition, targetPosition, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // wait for animation to finish
        //float time = (go.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length + 1f) / 6f;
        float time = 1f;
        yield return new WaitForSeconds(time);

        // calculate damage
        // normal attack
        if (magicAttack == false)
        {
            if (state == BattleState.EnemyMove)
            {
                playerUnits[currentTarget].TakeAttackDamage(enemyUnits[moveIndex - playerUnits.Count]);
                characterHuds[currentTarget].GetComponent<CharacterHud>().SetData(playerUnits[currentTarget]);
            }
            if (state == BattleState.PlayerMove)
            {
                enemyUnits[currentTarget].TakeAttackDamage(playerUnits[moveIndex]);
            }
            //Sets the magic selected to false
            magicSelected = false;
        }
        // magic or skill attack
        if (magicAttack == true)
        {
            if (state == BattleState.PlayerMove)
            {
                int currentMagic = magicPanel.GetComponent<MagicPanel>().GetMagic();
                enemyUnits[currentTarget].TakeMagicDamage(playerUnits[moveIndex].Magics[currentMagic], playerUnits[moveIndex]);
                characterHuds[moveIndex].GetComponent<CharacterHud>().SetData(playerUnits[moveIndex]);
                //Sets the magic selected to false
                magicSelected = false;
            }
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i] != null)
            {
                if (enemyUnits[i].HP <= 0)
                {
                    Destroy(battleSprites[i + playerUnits.Count]);
                }
            }
        }

        // go back to starting position
        elapsedTime = 0;
        //go.GetComponent<Animator>().SetBool("isRunning", true);
        go.transform.localRotation *= Quaternion.Euler(0, 180, 0);
        while (elapsedTime < seconds)
        {
            go.transform.localPosition = Vector3.Lerp(targetPosition, startingPosition, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        go.transform.localRotation *= Quaternion.Euler(0, 180, 0);

        CheckWin();

        // roll next turn if the battle is not over
        if (state != BattleState.Result)
        {
            state = BattleState.TurnRoll;
        }
    }

    // determine the next character to move
   private int NextMove()
    {
        targetSelection.numberOfEnemies = enemyUnits.Count - 1;
        float value = 0f;
        int index = 0;
        bool player = false;

        // loop through both lists and save the highest spd value as well as its index in the temp list
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i] != null)
            {
                if (playerUnits[i].Spd > value)
                {
                    if (!tempList.Contains(i))
                    {
                        value = playerUnits[i].Spd;
                        index = i;
                        player = true;
                    }
                }
            }
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i] != null)
            {
                if (enemyUnits[i].Spd > value)
                {
                    if (!tempList.Contains(i + playerUnits.Count))
                    {
                        value = enemyUnits[i].Spd;
                        index = i + playerUnits.Count;
                        player = false;
                    }
                }
            }
        }

        // if every character has moved, clear the list and call the function again
        if (tempList.Contains(index) || battleSprites[index] == null)
        {
            tempList.Clear();
            return NextMove();
        }
        else
        {
            tempList.Add(index);
            // if the index is less than the number of elements in player units, that means it's the player's turn
            if (player == true)
            {
                pMoved = false;
                actionSeletor.SetActive(true);
                turnIndicator.SetActive(true);
                turnIndicator.transform.localPosition = playerSpawnPositions[index] + new Vector3(0f, 220f, 0f);
                stateChange(BattleState.PlayerAction);
                return index;
            }
            // otherwise it would be the enemy's turn which attacks a random character
            else
            {
                eMoved = false;
                stateChange(BattleState.EnemyMove);
                return index;
            }
        }
    }

    // victory menu
    private void CheckWin()
    {
        int deadEnemy = 0;
        int deadPlayer = 0;

        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i].HP <= 0)
            {
                deadPlayer++;
            }
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (enemyUnits[i].HP <= 0)
            {
                deadEnemy++;
            }
        }

        // if all enemies are killed, exit out of the battle world
        if (enemyUnits.Count == deadEnemy)
        {
            state = BattleState.Result;
        }

        // if all players are killed, go back to main menu (just for now)
        if (playerUnits.Count == deadPlayer)
        {
            SceneManager.LoadScene("TitleScene");
        }
    }

    public void SetCharacters(List<Character> characters)
    {
        playerUnits = characters;
    }

    private void CreateEnemy()
    {
        // add enemy characters to the battle scene
        for (int i = 0; i < GameManager.instance.EBase.Count; i++)
        {
            enemyUnits.Add(new Character(GameManager.instance.EBase[i], GameManager.instance.Elevel[i]));
        }
    }
}
