using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TacticsUI : MonoBehaviour
{
    [SerializeField]
    private Sprite[] playerIcons;

    private Image[] slots;

    private int currentIndex;

    private int replaceIndex;

    private string replaceText1;

    private string replaceText2;

    private bool replaceMode;

    // Start is called before the first frame update
    void Start()
    {
        slots = GetComponentsInChildren<Image>();
        currentIndex = 0;
        replaceIndex = 0;
        replaceMode = false;
    }

    // Update is called once per frame
    void Update()
    {
        slots[currentIndex + 1].color = new Color(0f, 0f, 0f, 0.5f);
        // choose which slot you want to replace
        if (replaceMode == false)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow) && currentIndex < 8)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex++;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && currentIndex > 0)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex--;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && currentIndex > 2)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex -= 3;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && currentIndex < 6)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex += 3;
            }
            if (Input.GetKeyDown(KeyCode.Return) && GameManager.instance.battlePosition[currentIndex] != "")
            {
                replaceIndex = currentIndex;
                replaceText1 = GameManager.instance.battlePosition[currentIndex];
                replaceMode = true;
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                gameObject.SetActive(false);
                Time.timeScale = 1f;
            }
        }
        // choose which slot you want to replace with
        else
        {
            slots[replaceIndex + 1].color = Color.blue;
            if (Input.GetKeyDown(KeyCode.RightArrow) && currentIndex < 8)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex++;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && currentIndex > 0)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex--;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && currentIndex > 2)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex -= 3;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && currentIndex < 6)
            {
                slots[currentIndex + 1].color = Color.white;
                currentIndex += 3;
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                slots[replaceIndex + 1].color = Color.white;
                replaceText2 = GameManager.instance.battlePosition[currentIndex];
                GameManager.instance.battlePosition[replaceIndex] = replaceText2;
                GameManager.instance.battlePosition[currentIndex] = replaceText1;
                replaceMode = false;
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                slots[replaceIndex + 1].color = Color.white;
                replaceMode = false;
            }
        }

        // set the slot that is occupied to its character icon
        // if the slot is empty, set the slot to a white box
        for (int i = 0; i < GameManager.instance.battlePosition.Length; i++)
        {
            if (GameManager.instance.battlePosition[i] == "Arthur")
            {
                slots[i + 1].sprite = playerIcons[0];
            }
            else if (GameManager.instance.battlePosition[i] == "Wendy")
            {
                slots[i + 1].sprite = playerIcons[1];
            }
            else if (GameManager.instance.battlePosition[i] == "Zed")
            {
                slots[i + 1].sprite = playerIcons[2];
            }
            else if (GameManager.instance.battlePosition[i] == "Arlia")
            {
                slots[i + 1].sprite = playerIcons[3];
            }
            else if (GameManager.instance.battlePosition[i] == "")
            {
                slots[i + 1].sprite = null;
            }
        }
    }
}
