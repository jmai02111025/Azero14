using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest
{
    // type of quests, including killing x monsters, collecting x items, or talk to a specific NPC.
    public enum QuestType
    {
        Kill,
        Collect,
        Talk,
    }

    // reward items
    [System.Serializable]
    public class rewardItems
    {
        public Item item;
        public int amount;
    }

    public QuestType type;

    public CharacterBase.NPCType TargetType;

    public string questName;

    // amount is 0 when quest type is talk 
    public int amount;
    // target monster to kill or target items to collect
    public int targetId;
    // who to talk to when the quest is completed to receive rewards
    public int NPCId;
    // rewards
    public int coins;
    public List<rewardItems> items = new List<rewardItems>();
    // description
    [TextArea]
    public string description;
}
