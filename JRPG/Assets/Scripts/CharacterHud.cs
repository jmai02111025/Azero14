using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHud : MonoBehaviour
{
    #region Initial Variables
    [SerializeField]
    private HPBar hpBar;

    [SerializeField]
    private MPBar mpBar;

    [SerializeField]
    private Text hpText;

    [SerializeField]
    private Text mpText;
    #endregion
    // Fill in the stats for the character huds
    public void SetData(Character character)
    {
        hpBar.SetHP((float)character.HP / character.MaxHP);
        hpText.text = (character.HP).ToString();

        mpBar.SetMP((float)character.MP / character.MaxMP);
        mpText.text = (character.MP).ToString();
    }
}
