using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Familiarity : MonoBehaviour
{
    // A character has made a normal attack, he/she is more familiar with attacking enemies
    public void UpdateStrFamiliarity(Character character)
    {
        Debug.Log("Current str: " + character.Str);
        Debug.Log("Current Familiarity: " + character.StrFamiliarity);
        character.StrFamiliarity--;
        if (character.StrFamiliarity <= 0)
        {
            character.StrBonus += 1;
            character.StrFamiliarity = character.GetFamiliarity(character.Str);
        }
        Debug.Log("New str: " + character.Str);
        Debug.Log("New Familiarity: " + character.StrFamiliarity);
    }

    // A character has taken physical damage, he/she is more familiar with guarding attacks
    public void UpdateDefFamiliarity(Character character)
    {
        Debug.Log("Current def: " + character.Def);
        Debug.Log("Current Familiarity: " + character.DefFamiliarity);
        character.DefFamiliarity--;
        if (character.DefFamiliarity <= 0)
        {
            character.DefBonus += 1;
            character.DefFamiliarity = character.GetFamiliarity(character.Def);
        }
        Debug.Log("New def: " + character.Def);
        Debug.Log("New Familiarity: " + character.DefFamiliarity);
    }

    // A character has made a skill attack, he/she is more familiar with using skills
    public void UpdateMgsFamiliarity(Character character)
    {
        Debug.Log("Current mgs: " + character.Mgs);
        Debug.Log("Current Familiarity: " + character.MgsFamiliarity);
        character.MgsFamiliarity--;
        if (character.MgsFamiliarity <= 0)
        {
            character.MgsBonus += 1;
            character.MgsFamiliarity = character.GetFamiliarity(character.Mgs);
        }
        Debug.Log("New mgs: " + character.Mgs);
        Debug.Log("New Familiarity: " + character.MgsFamiliarity);
    }

    // A character has taken skill damage, he/she is more familiar with guarding attacks
    public void UpdateMdfFamiliarity(Character character)
    {
        Debug.Log("Current mdf: " + character.Mdf);
        Debug.Log("Current Familiarity: " + character.MdfFamiliarity);
        character.MdfFamiliarity--;
        if (character.MdfFamiliarity <= 0)
        {
            character.MdfBonus += 1;
            character.MdfFamiliarity = character.GetFamiliarity(character.Mdf);
        }
        Debug.Log("New mdf: " + character.Mdf);
        Debug.Log("New Familiarity: " + character.MdfFamiliarity);
    }

    // A character has slained an enemy, he/she is more familiar with his/her weapon
    public void UpdateWeaponFamiliarity(Character character)
    {
        Debug.Log(character.Base.CharacterName);
        Debug.Log("Current Weapon Familiarity: " + character.WeaponFamiliarity);
        character.WeaponFamiliarity++;
        character.GetMagic();
        Debug.Log("New Weapon Familiarity: " + character.WeaponFamiliarity);
    }

    // A character has used a skill, he/she is more familiar with his/her skill
    public void updateMagicFamiliarity(Magic magic)
    {
        Debug.Log(magic.Base.MagicName);
        Debug.Log("Current Magic Familiarity: " + magic.magicFamiliarity);
        magic.magicFamiliarity++;
        Debug.Log("New Magic Familiarity: " + magic.magicFamiliarity);
    }
}
