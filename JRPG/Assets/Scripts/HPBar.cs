using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HPBar : MonoBehaviour
{
    [SerializeField]
    private GameObject health;

    public void SetHP(float HP)
    {
        health.transform.localScale = new Vector3(HP, 1f);
    }
}
