using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldMovementInformation : MonoBehaviour
{
    #region Initial Variables
    [SerializeField]
    int whichMovementFunction;
    [SerializeField]
    List<GameObject> allMovementPoints = new List<GameObject>();
    int whichPointMovingTowards;
    float distanceBetweenPoints;
    Vector2 startingPosition;
    Vector2 endPosition;
    public float movementMultiplier;
    bool canMove;
    [SerializeField]
    int secondsBetweenMovement;
    #endregion
    void Start()
    {
        distanceBetweenPoints = 0;
        whichPointMovingTowards = 1;
        startingPosition = allMovementPoints[0].transform.position;
        endPosition = allMovementPoints[1].transform.position;
        canMove = true;
    }

    void Update()
    {
        //If the enemy can move, then they begin to Lerp between positions
        if (canMove)
        {
            gameObject.GetComponent<Animator>().SetBool("isMoving", true);
            LerpBetweenPositions();
        }
        if (endPosition.x > startingPosition.x)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
    }

    void LerpBetweenPositions()
    {
        //Checks to see if the enemy has arrived at its position
        if (distanceBetweenPoints < 1)
        {
            //Changes the lerp function depending on which movement function has been chosen
            switch (whichMovementFunction)
            {
                //Uses a vector2 lerp function to lerp the enemy between points
                case 1:
                    transform.position = Vector2.Lerp(startingPosition, endPosition, Mathf.Sin((distanceBetweenPoints * Mathf.PI) / 2));
                    break;
                case 2:
                    transform.position = Vector2.Lerp(startingPosition, endPosition, 1 - Mathf.Cos((distanceBetweenPoints * Mathf.PI) / 2));
                    break;
                case 3:
                    transform.position = Vector2.Lerp(startingPosition, endPosition, Mathf.Sin(-(Mathf.Cos(Mathf.PI * distanceBetweenPoints) - 1) / 2));
                    break;
                case 4:
                    transform.position = Vector2.Lerp(startingPosition, endPosition, 1 - Mathf.Pow(1 - distanceBetweenPoints, 5));
                    break;
            }
            //After the lerp, then the distance between points is added to
            distanceBetweenPoints += Time.deltaTime * movementMultiplier;
        }
        else
        {
            //If the enemy has reached its destination, then its can move is changed to false, and the time between moving is started
            gameObject.GetComponent<Animator>().SetBool("isMoving", false);
            canMove = false;
            StartCoroutine(TimeBetweenMoving());
        }
    }

    //When called, it cycles the point being moved towards to the next one, or makes it the first one if it has reached the end of the list
    void MoveToNextSpace()
    {
        if (whichPointMovingTowards == allMovementPoints.Count - 1)
        {
            startingPosition = allMovementPoints[whichPointMovingTowards].transform.position;
            whichPointMovingTowards = 0;
            endPosition = allMovementPoints[whichPointMovingTowards].transform.position;
        }
        else
        {
            startingPosition = allMovementPoints[whichPointMovingTowards].transform.position;
            whichPointMovingTowards += 1;
            endPosition = allMovementPoints[whichPointMovingTowards].transform.position;

        }
        //Resets the distancebetweenpoints and canmove to trued
        distanceBetweenPoints = 0;
        canMove = true;
    }

    //The coroutine is called, and when it is done, it calls the movetonextspace function
    private IEnumerator TimeBetweenMoving()
    {
        yield return new WaitForSecondsRealtime(secondsBetweenMovement);
        MoveToNextSpace();
    }
}
