﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TestAI", menuName = "AI/Test AI")]
public class TestAI : BaseAI
{
    // Data here!
    public class MapData
    {
        public GameManager.SensorData state;

        public MapData current;
        public MapData up;
        public MapData left;
        public MapData right;
        public MapData down;

        public int walkableSpace = 0;    
    }

    public List<MapData> maps = new List<MapData>();
    public bool diamondFound = false;
    public bool goalFound = false;

    public bool upOffest = false;
    public bool downOffeset = false;
    public bool leftOffset = false;
    public bool rightOffest = false;

    public Vector2 currentPosition;
    public List<Vector2> traveledToPositions = new List<Vector2>();

    List<GameManager.Direction> movesToCurrent = new List<GameManager.Direction>();

    List<GameManager.Direction> movesToGoal = new List<GameManager.Direction>();


    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        // scan map and store data information about surrounding objects.
        MapData map = new MapData();
        if (map.current == null)
        {
            map.current = new MapData();
            map.current.state = UseSensor(GameManager.Direction.Current);
            traveledToPositions.Add(currentPosition);
        }

        if (map.up == null)
        {
            map.up = new MapData();
            map.up.state = UseSensor(GameManager.Direction.Up);
        }

        if (map.down == null)
        {
            map.down = new MapData();
            map.down.state = UseSensor(GameManager.Direction.Down);
        }

        if (map.left == null)
        {
            map.left = new MapData();
            map.left.state = UseSensor(GameManager.Direction.Left);
        }

        if (map.right == null)
        {
            map.right = new MapData();
            map.right.state = UseSensor(GameManager.Direction.Right);
        }

        #region OffGrid
        if ((map.up.state & GameManager.SensorData.OffGrid) != 0)
        {
            upOffest = true;
        }
        else
        {
            upOffest = false;
        }

        if ((map.down.state & GameManager.SensorData.OffGrid) != 0)
        {
            downOffeset = true;
        }
        else
        {
            downOffeset = false;
        }

        if ((map.left.state & GameManager.SensorData.OffGrid) != 0)
        {
            leftOffset = true;
        }
        else
        {
            leftOffset = false;
        }

        if ((map.right.state & GameManager.SensorData.OffGrid) != 0)
        {
            rightOffest = true;
        }
        else
        {
            rightOffest = false;
        }
        #endregion

        #region Goal Spotted
        // Goal Spotted
        // if the AI already has the diamond, go to the goal and end the game
        // if not, save the position of the goal for future use
        if ((map.up.state & GameManager.SensorData.Goal) != 0)
        {
            if (diamondFound)
            {
                aMoves.Add(GameManager.Direction.Up);
                return CombatantAction.Move;
            }
            else
            {
                if (!goalFound)
                {
                    movesToGoal = convertList(movesToCurrent);
                    movesToGoal.Add(GameManager.Direction.Up);
                    goalFound = true;
                }
            }
        }
        else if ((map.down.state & GameManager.SensorData.Goal) != 0)
        {
            if (diamondFound)
            {
                aMoves.Add(GameManager.Direction.Down);
                return CombatantAction.Move;
            }
            else
            {
                if (!goalFound)
                {
                    movesToGoal = convertList(movesToCurrent);
                    movesToGoal.Add(GameManager.Direction.Down);
                    goalFound = true;
                }
            }
        }
        else if ((map.left.state & GameManager.SensorData.Goal) != 0)
        {
            if (diamondFound)
            {
                aMoves.Add(GameManager.Direction.Left);
                return CombatantAction.Move;
            }
            else
            {
                if (!goalFound)
                {
                    movesToGoal = convertList(movesToCurrent);
                    movesToGoal.Add(GameManager.Direction.Left);
                    goalFound = true;
                }
            }
        }
        else if ((map.right.state & GameManager.SensorData.Goal) != 0)
        {
            if (diamondFound)
            {
                aMoves.Add(GameManager.Direction.Right);
                return CombatantAction.Move;
            }
            else
            {
                if (!goalFound)
                {
                    movesToGoal = convertList(movesToCurrent);
                    movesToGoal.Add(GameManager.Direction.Right);
                    goalFound = true;
                }
            }
        }
        #endregion

        #region foundDiamond
        // Diamond Spotted
        if ((map.up.state & GameManager.SensorData.Diamond) != 0)
        {
            diamondFound = true;
            aMoves.Add(GameManager.Direction.Up);
            movesToCurrent.Add(GameManager.Direction.Up);
            currentPosition.y += 1;
            return CombatantAction.Move;
        }
        else if ((map.down.state & GameManager.SensorData.Diamond) != 0)
        {
            diamondFound = true;
            aMoves.Add(GameManager.Direction.Down);
            movesToCurrent.Add(GameManager.Direction.Down);
            currentPosition.y -= 1;
            return CombatantAction.Move;
        }
        else if ((map.left.state & GameManager.SensorData.Diamond) != 0)
        {
            diamondFound = true;
            aMoves.Add(GameManager.Direction.Left);
            movesToCurrent.Add(GameManager.Direction.Left);
            currentPosition.x -= 1;
            return CombatantAction.Move;
        }
        else if ((map.right.state & GameManager.SensorData.Diamond) != 0)
        {
            diamondFound = true;
            aMoves.Add(GameManager.Direction.Right);
            movesToCurrent.Add(GameManager.Direction.Right);
            currentPosition.x += 1;
            return CombatantAction.Move;
        }
        #endregion

        #region EndCondition
        // if the AI knows where the goal is and it has the diamond, go to the goal to end the game
        if (diamondFound && goalFound)
        {
            for (int i = movesToCurrent.Count - 1; i > -1; i--)
            {
                switch (movesToCurrent[i])
                {
                    case GameManager.Direction.Up:
                        aMoves.Add(GameManager.Direction.Down);
                        break;
                    case GameManager.Direction.Down:
                        aMoves.Add(GameManager.Direction.Up);
                        break;
                    case GameManager.Direction.Left:
                        aMoves.Add(GameManager.Direction.Right);
                        break;
                    case GameManager.Direction.Right:
                        aMoves.Add(GameManager.Direction.Left);
                        break;
                    case GameManager.Direction.Current:
                        aMoves.Add(GameManager.Direction.Current);
                        break;
                    default:
                        break;
                }
            }
            for (int i = 0; i < movesToGoal.Count; i++)
            {
                aMoves.Add(movesToGoal[i]);
            }
            return CombatantAction.Move;
        }
        #endregion

        #region runFromBomb
        // if a bomb is nearby, attempt to turn away from it
        if ((map.up.state & GameManager.SensorData.Bomb) != 0 || (map.down.state & GameManager.SensorData.Bomb) != 0 || (map.left.state & GameManager.SensorData.Bomb) != 0 || (map.right.state & GameManager.SensorData.Bomb) != 0 || (map.current.state & GameManager.SensorData.Bomb) != 0)
        {
            if ((map.up.state & GameManager.SensorData.Clear) != 0 && !upOffest && (map.up.state & GameManager.SensorData.Bomb) == 0)
            {
                movesToCurrent.Add(GameManager.Direction.Up);
                currentPosition.y += 1;
                aMoves.Add(GameManager.Direction.Up);
            }
            else if ((map.down.state & GameManager.SensorData.Clear) != 0 && !downOffeset && (map.down.state & GameManager.SensorData.Bomb) == 0)
            {
                movesToCurrent.Add(GameManager.Direction.Down);
                currentPosition.y -= 1;
                aMoves.Add(GameManager.Direction.Down);
            }
            else if ((map.left.state & GameManager.SensorData.Clear) != 0 && !leftOffset && (map.left.state & GameManager.SensorData.Bomb) == 0)
            {
                movesToCurrent.Add(GameManager.Direction.Left);
                currentPosition.x -= 1;
                aMoves.Add(GameManager.Direction.Left);
            }
            else if ((map.right.state & GameManager.SensorData.Clear) != 0 && !rightOffest && (map.right.state & GameManager.SensorData.Bomb) == 0)
            {
                movesToCurrent.Add(GameManager.Direction.Right);
                currentPosition.x += 1;
                aMoves.Add(GameManager.Direction.Right);
            }
            return CombatantAction.Move;
        }
        #endregion

        #region placeBomb
        // if an enemy is nearby, place a bomb attempting to kill it
        if ((map.up.state & GameManager.SensorData.Enemy) != 0 || (map.down.state & GameManager.SensorData.Enemy) != 0 || (map.left.state & GameManager.SensorData.Enemy) != 0 || (map.right.state & GameManager.SensorData.Enemy) != 0 || (map.current.state & GameManager.SensorData.Enemy) != 0)
        {
            aBombTime = 5;
            return CombatantAction.DropBomb;
        }
        #endregion

        // Check for places to go
        if ((map.up.state & GameManager.SensorData.Clear) != 0 && !upOffest && !traveledToPositions.Contains(new Vector2(currentPosition.x, currentPosition.y + 1)))
        {
            aMoves.Add(GameManager.Direction.Up);
            movesToCurrent.Add(GameManager.Direction.Up);
            currentPosition.y += 1;
            return CombatantAction.Move;
        }
        else if ((map.down.state & GameManager.SensorData.Clear) != 0 && !downOffeset && !traveledToPositions.Contains(new Vector2(currentPosition.x, currentPosition.y - 1)))
        {
            aMoves.Add(GameManager.Direction.Down);
            movesToCurrent.Add(GameManager.Direction.Down);
            currentPosition.y -= 1;
            return CombatantAction.Move;
        }
        else if ((map.left.state & GameManager.SensorData.Clear) != 0 && !leftOffset && !traveledToPositions.Contains(new Vector2(currentPosition.x - 1, currentPosition.y)))
        {
            aMoves.Add(GameManager.Direction.Left);
            movesToCurrent.Add(GameManager.Direction.Left);
            currentPosition.x -= 1;
            return CombatantAction.Move;
        }
        else if ((map.right.state & GameManager.SensorData.Clear) != 0 && !rightOffest && !traveledToPositions.Contains(new Vector2(currentPosition.x + 1, currentPosition.y)))
        {
            aMoves.Add(GameManager.Direction.Right);
            movesToCurrent.Add(GameManager.Direction.Right);
            currentPosition.x += 1;
            return CombatantAction.Move;
        }
        else
        {
            GameManager.Direction tempDirection = movesToCurrent[movesToCurrent.Count - 1];
            GameManager.Direction newDirection;
            switch (tempDirection)
            {
                case GameManager.Direction.Up:
                    newDirection = GameManager.Direction.Down;
                    currentPosition.y -= 1;
                    break;
                case GameManager.Direction.Down:
                    newDirection = GameManager.Direction.Up;
                    currentPosition.y += 1;
                    break;
                case GameManager.Direction.Left:
                    newDirection = GameManager.Direction.Right;
                    currentPosition.x += 1;
                    break;
                case GameManager.Direction.Right:
                    newDirection = GameManager.Direction.Left;
                    currentPosition.x -= 1;
                    break;
                case GameManager.Direction.Current:
                    newDirection = GameManager.Direction.Current;
                    break;
                default:
                    newDirection = GameManager.Direction.Current;
                    break;
            }
            aMoves.Add(newDirection);
            movesToCurrent.RemoveAt(movesToCurrent.Count - 1);
            return CombatantAction.Move;
        }
    }

    // convert the moves to current list to moves to goal list
    private List<GameManager.Direction> convertList(List<GameManager.Direction> Directions)
    {
        List<GameManager.Direction> tempDirection = new List<GameManager.Direction>();
        for (int i = 0; i < Directions.Count; i++)
        {
            tempDirection.Add(Directions[i]);
        }
        return tempDirection;
    }
}